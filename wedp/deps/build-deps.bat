pushd %2\deps

call %1 -startdir=none -arch=x64 -host_arch=x64
cmake . -B build
msbuild build/Project.sln -t:build -p:platform=x64 -p:configuration=%3 

popd