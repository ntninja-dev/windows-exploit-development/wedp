cmake_minimum_required(VERSION 3.16)

# BUILDING
# cmake . -B build
# msbuild build/Project.sln -t:rebuild -p:configuration=release -p:platform=x64

# set our output directory
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_LIST_DIR}/libs)

# set our specific compiler flags
set(CMAKE_CXX_FLAGS_RELEASE "/MT")
set(CMAKE_CXX_FLAGS_DEBUG "/MTd")
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")

# libfort 
set(FORT_ENABLE_TESTING OFF CACHE INTERNAL "")
add_subdirectory(libfort)

# asmtk (uses asmjit)
set(ASMJIT_STATIC TRUE)
set(ASMTK_STATIC TRUE)
add_subdirectory(asmtk)

# zydis
option(ZYDIS_BUILD_TOOLS "" OFF)
option(ZYDIS_BUILD_EXAMPLES "" OFF)
option(ZYDIS_BUILD_DOXYGEN "" OFF)
add_subdirectory(zydis)
