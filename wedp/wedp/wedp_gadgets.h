#pragma once
#include "wedp_cache.h"
#include "wedp_options.h"
#include "wedp_debug.h"

#include <Zydis/zydis.h>

#include <vector>

namespace wedp {

#define WEDP_ROP_DEPTH 5
#define WEDP_ROP_OFF_DEPTH 3
#define WEDP_JMP_DEPTH 2
#define WEDP_CALL_DEPTH 2

    enum {
        GADGET_INVALID = 0,
        GADGET_ROP = (1 << 0),
        GADGET_ROP_OFF = (1 << 1),
        GADGET_REDIRECT = (1 << 2),
        GADGET_STACKPIVOT = (1 << 3),
        GADGET_SEH = (1 << 4),
        GADGET_JOP = (1 << 5)
    };

    class WedpGadgetTree {
        public:
            WORD machine;
            ULONG offset = 0;
            ULONG treeDepth = 0;
            // ZydisDecodedInstruction instruction;
            ZydisDisassembledInstruction instruction;
            WedpGadgetTree* parent;
            std::list<WedpGadgetTree*> children;

            ~WedpGadgetTree();
    };

    struct WEDP_GADGET {
        ULONG type = 0;
        ULONG len = 0;
        ULONG64 address = 0;
        ULONG64 rva = 0;
        ULONG64 moduleBase = 0;
        std::string mnemonic;
        // std::vector<BYTE> bytes;


        // Used for sort operations
        bool operator < (const WEDP_GADGET& other) const
        {
            // In order to satisy the stdlib requirements for sort we need the || with the ==
            // for the next option to check
            return ((this->len < other.len) ||
                ((this->len == other.len) && (this->address < other.address)));
        }
    };


    class WedpGadgetSearch {

        public:
            WedpGadgetSearch(const WEDP_MEMORY* mem, const WEDP_MODULE* mod);
            ~WedpGadgetSearch();
            void start_search();
            bool wait_completion();

            // std::list<WEDP_GADGET> *gadgets;
            std::list<WEDP_GADGET> gadgets;
            
        private:
            const WEDP_MEMORY* memory;
            const WEDP_MODULE* module;
            HANDLE search_thread;
            HANDLE search_complete;

            static void WINAPI run_search(LPVOID tp);
            void parse_gadget_tree(WedpGadgetTree* root);
            ULONG classify_gadget(WedpGadgetTree* curr, ULONG baseType);
            void insert_gadget(WedpGadgetTree* curr, ULONG type);
    };


    // Used to find different gadgets that are useful for exploitation
    class WedpGadgetHelper : public  WedpCache<WEDP_GADGET, ULONG64> {

        public:
            std::string getGadgetTable(WedpGenericOptions opts);

            bool insert(WEDP_GADGET val) override;
            bool exists(ULONG64 val) override;
            const WEDP_GADGET *getvalue(ULONG64 val) override;
            bool remove(ULONG64 val) override;
            std::vector<WEDP_GADGET> filter(WedpGenericOptions opts) override;
            bool checkcachevalidity() override;
            bool updatecache() override;
        
        private:
            ULONG64 hProcess;
    };

} // namespace wedp