#pragma once
#include <windows.h>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "wedp_version.h"


namespace wedp {

    // Prototype struct for generic options to filter against
    struct WedpGenericOptions {
        // Generics
        ULONG64 resultCount = (ULONG64)(-1);
        std::string outputFile = std::string();
        std::string outputDir = std::string();

        // Module
        ULONG modulesFlags = 0;;
        std::set<std::string> modules = std::set<std::string>();

        // Address
        ULONG addressProtections = 0xffffffff;
        ULONG addressCharacteristics = 0;
        std::set<BYTE> addressBadChars = std::set<BYTE>();

        // Gadget
        ULONG gadgetType = 0;
        std::string gadgetRegister = std::string();

        // WedpGenericOptions();
        std::string output();

        enum {
            MODULE_FLAG_INCLUDE = (1 << 0),
            MODULE_FLAG_EXCLUDE = (1 << 1),
            MODULE_ASLR_ENABLED = (1 << 2),
            MODULE_ASLR_DISABLED = (1 << 3),
            MODULE_DEP_ENABLED = (1 << 4),
            MODULE_DEP_DISABLED = (1 << 5),
            MODULE_SAFESEH_ENABLED = (1 << 6),
            MODULE_SAFESEH_DISABLED = (1 << 7),
            MODULE_CFG_ENABLED = (1 << 8),
            MODULE_CFG_DISABLED = (1 << 9),
            MODULE_SYSTEM_IMAGE_TRUE = (1 << 10),
            MODULE_SYSTEM_IMAGE_FALSE = (1 << 11),
        };

        enum {
            ADDRESS_NONULL = (1 << 0),
            ADDRESS_ASCIIPRINT = (1 << 1),
            ADDRESS_ASCIIALPHANUM = (1 << 2)
        };

        static const std::map<std::string, ULONG> addProtFlags;
    };


    // Hold global options for the plugin
	class WedpOptions {

	    public:
		    WedpOptions();
		    USHORT majorVersion = WEDP_MAJOR_VERSION;
		    USHORT minorVersion = WEDP_MINOR_VERSION;
            USHORT patchVersion = WEDP_PATCH_VERSION;
            WedpGenericOptions g_opts;

            std::string GetVersionString(VOID);
	};
    
} // namespace wedp