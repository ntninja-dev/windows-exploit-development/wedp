#pragma once
#include <engextcpp.hpp>

class WedpDbgEngCallbacks : public DebugBaseEventCallbacks {

private:
    long RefCount;

public:
    WedpDbgEngCallbacks() { RefCount = 1; }

    // IUnknown
    STDMETHOD_(ULONG, AddRef)();
    STDMETHOD_(ULONG, Release)();

    STDMETHOD(GetInterestMask)(THIS_ PULONG Mask);
    STDMETHOD(LoadModule)(THIS_ ULONG64 ImageFileHandle, ULONG64 BaseOffset, ULONG ModuleSize,
        PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp);
    STDMETHOD(UnloadModule)(THIS_ PCSTR ImageBaseName, ULONG64 BaseOffset);
};
