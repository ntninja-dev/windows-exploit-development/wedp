#pragma once
#ifdef _DEBUG
// Nice debugging macros that wont compile into release builds

#include <windows.h>
#include <stdio.h>

#define MAX_DBG_STRLEN 1024

// The actual function that formats the string and calls the
// proper output function
static __inline  VOID
OutputDebugStringFormat(LPCSTR fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    CHAR szErrorString[MAX_DBG_STRLEN] = { 0 };
    _vsnprintf_s(szErrorString, MAX_DBG_STRLEN, fmt, args);
    OutputDebugStringA(szErrorString);
    va_end(args);
    return;
}

// Macro that adds [WEDP DEBUG] before any call
#define DBGPRINT(fmt, ...) OutputDebugStringFormat("[WEDP DEBUG]: " fmt, __VA_ARGS__)

#else
// Do nothing on release
#define DBGPRINT(...) while (0) {}
#endif