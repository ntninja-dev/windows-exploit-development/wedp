#pragma once
#include <engextcpp.hpp>
#include <sstream>
#include <string>
#include <queue>

namespace wedp {

    // Helper class that exposes the ability to execute native
    // debugger commands and get the output
    class WedpExec {
        public:
            std::string exec(std::string);

        // Execute a single debugger command and gather the output
        // This class is a COM interface that is registered with
        // the client it creates to collect output from the dbgeng
        // This is nested so that it can be controlled and used for only a 
        // single command pre object
        class WedpExecCmd : public IDebugOutputCallbacks {

            private:
                const static std::string endOutputDelim;
                HANDLE executionCompleteEvent;
                std::stringstream outStream;

                // dbgeng interfaces
                IDebugClient* client;
                IDebugControl* control;

                // original callback function pointer
                PDEBUG_OUTPUT_CALLBACKS orig;

            public:
                WedpExecCmd();
                ~WedpExecCmd();

                // IUnknown interface methodes
                STDMETHOD(QueryInterface)(THIS_ _In_ REFIID InterfaceId, _Out_ PVOID* Interface);
                STDMETHOD_(ULONG, AddRef)(THIS);
                STDMETHOD_(ULONG, Release)(THIS);

                // IDebugOutputCallbacks interface methods
                STDMETHOD(Output)(THIS_ _In_ ULONG Mask, _In_ PCSTR Text);

                // Method to actaully execute the input command
                // NOTE: figure out if we can block output from hitting the debugger in nested commands
                // like !address wiht /c:. We still get the output, but it is also output to the console
                // when it executes
                std::string exec(std::string);
        };
    };
    
} // namespace wedp