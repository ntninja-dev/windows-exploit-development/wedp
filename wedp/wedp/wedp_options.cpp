#include <sstream>

#include "wedp_options.h"



namespace wedp {

    const std::map<std::string, ULONG> WedpGenericOptions::addProtFlags = {
        { "R",  PAGE_READONLY | PAGE_READWRITE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY | PAGE_WRITECOPY },
        { "W",  PAGE_READWRITE | PAGE_WRITECOPY | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY },
        { "X",  PAGE_EXECUTE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY },
        { "RW", PAGE_READWRITE | PAGE_WRITECOPY | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY },
        { "RX", PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY},
        { "WX", PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY},
        { "RWX", PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY}
    };

    std::string WedpGenericOptions::output()
    {
        std::stringstream ss;
        ss << "OPTIONS\n";
        ss << "\tRESULT COUNT: 0x" << std::hex << this->resultCount << "\n";
        ss << "\tOUTPUT DIR: " << this->outputDir << "\n";
        ss << "\tOUTPUT FILE: " << this->outputFile << "\n";
        ss << "\tMOD FLAGS: 0x" << std::hex << this->modulesFlags << "\n";
        ss << "\tMODULES: \n";

        for (std::string m : this->modules) {
            ss << "\t\t" << m << "\n";
        }

        ss << "\tADDR PROTS: 0x" << std::hex << this->addressProtections << "\n";
        ss << "\tADDR CHARS: 0x" << std::hex << this->addressCharacteristics << "\n";

        ss << "\tADDR BAD CHARS:\n";
        for (BYTE c : this->addressBadChars) {
            ss << "\t\t0x" << std::hex << c << "\n";
        }

        return ss.str();
    }


    // Set default global options here
    WedpOptions::WedpOptions()
    {
        this->majorVersion = WEDP_MAJOR_VERSION;
        this->minorVersion = WEDP_MINOR_VERSION;
        this->patchVersion = WEDP_PATCH_VERSION;

        // We can set default global options here
        this->g_opts = WedpGenericOptions();       
    }

    // Output the plugin version string
    std::string WedpOptions::GetVersionString(VOID)
    {
        static const CHAR WedpAsciiArt[] = \
            "__          ________ _____  _____      \n" \
            "\\ \\        / /  ____|  __ \\|  __  \\    \n" \
            " \\ \\  /\\  / /| |__  | |  | | |__) |    \n" \
            "  \\ \\/  \\/ / |  __| | |  | |  ___/     \n" \
            "   \\  /\\  /  | |____| |__| | |         \n" \
            "    \\/  \\/   |______|_____/|_|         \n" \
            "          VERSION: ";

        std::stringstream ss;
        ss << WedpAsciiArt << WEDP_BUILD_VERSION_STRING << std::endl;

        return ss.str();
    }

} // namespace wedp