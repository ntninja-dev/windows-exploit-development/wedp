#include <Windows.h>
#include <sstream>
#include <iomanip>

#include "wedp.h"
#include "wedp_text.h"


extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

	const std::string WedpText::ascii_upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	const std::string WedpText::ascii_lower = "abcdefghijklmnopqrstuvwxyz";
	const std::string WedpText::ascii_digit = "0123456789";
	const std::string WedpText::ascii_hex = "0123456789ABCDEFabcdef";

    // Flags used for generating patterns
    enum {
        UPPER = 0,
        LOWER = 1,
        DIGIT = 2
    };

    // Try to emulate a python like generator by tracking state in private variables
    class WedpPatternGenerator {
       
        private:
            UCHAR upper_ind = 0;
            UCHAR lower_ind = 0;
            UCHAR digit_ind = 0;
            UCHAR type_ind = 0;
            ULONG count = 0;
           
        public:
            static const ULONG max = 26 * 26 * 10 * 3;
            
            // Get the next character in the pattern
            char next() 
            {
                char ret = 0;
                if (this->count > this->max) {
                    throw "Max Pattern Exceeded";
                }

                switch (this->type_ind) {
                    case UPPER:
                        ret = WedpText::ascii_upper[this->upper_ind];
                        break;
                    case LOWER:
                        ret = WedpText::ascii_lower[this->lower_ind];
                        break;
                    case DIGIT:
                        ret = WedpText::ascii_digit[this->digit_ind++];
                        this->digit_ind %= WedpText::ascii_digit.size();
                        if (0 == this->digit_ind) {
                            this->lower_ind++;
                            this->lower_ind %= WedpText::ascii_lower.size();

                            if (0 == this->lower_ind) {
                                this->upper_ind++;
                            }
                        }
                    default:
                        break;
                }

                this->type_ind++;
                this->type_ind %= 3;

                this->count++;
                return ret;
            }
    };

    // Generate a metasploit pattern
    std::string WedpText::generatePattern(ULONG len, ULONG width)
    {
        WedpPatternGenerator gen;
        std::stringstream pattern;

        for (ULONG i = 0; i < len && i < gen.max; i++) {
            pattern.put(gen.next());
            if (width && 0 == (i+1) % width) pattern.put('\n');
        }

        pattern.flush();
        return pattern.str();
    }

    // Find the offset of input in a metaxploit pattern
	ULONG WedpText::patternOffset(std::string pattern)
	{
		// Must convert the input to ascii prior to calling this
		std::string maxpat = this->generatePattern(WedpPatternGenerator::max);
        SIZE_T offset_ret = 0;

		offset_ret = maxpat.find(pattern);
		if (std::string::npos == offset_ret) {
			std::reverse(pattern.begin(), pattern.end());
            offset_ret = maxpat.find(pattern);
		}

		if (std::string::npos == offset_ret) {
			offset_ret = ULONG_MAX;
		}

		return (ULONG)offset_ret;
	}

    // Convert a hex string into an ascii string 
    // ex "41414141" -> "AAAA"
	std::string WedpText::hexToAscii(std::string hex)
	{
		std::string ascii = std::string();
		std::size_t hexlen = 0;

		if (0 == hex.compare(0, 2, "0x")) {
			hex.erase(0, 2);
		}

		if (0 != (hex.size() % 2) ||
			std::string::npos != hex.find_first_not_of(this->ascii_hex))
		{
			goto cleanup;
		}


		hexlen = hex.size() / 2;

		for (size_t i = 0; i < hexlen; i++) {
			uint32_t s = 0;
			std::stringstream ss;
			ss << std::hex << hex.substr(i * 2, 2);
			ss >> s;

			ascii.push_back(static_cast<unsigned char>(s));
		}
	cleanup:
		return ascii;
	}


    // Convert a vector of bytes into a hex string with a specified delimiter
    // ex {0x41, x41 } -> "\x41\x41"
    std::string WedpText::bytesToHex(std::vector<BYTE> bytes, std::string delim)
	{
		std::stringstream ss;
		for (BYTE b: bytes) {
			ss << delim << std::hex << std::setfill('0') << std::setw(2) << static_cast<unsigned>(b);
		}
		return ss.str();
	}

    std::vector<BYTE> WedpText::hexToBytes(std::string hex)
    {
        std::vector<BYTE> ret;
        if (0 != (hex.size() % 4)) {
            goto cleanup;
        }

        for (size_t i = 0; i < hex.size(); i += 4) {
            ULONG  b = 0;
            std::string curr = hex.substr(i + 2, 2);
            b = std::stoul(curr, nullptr, 16);
            ret.push_back(static_cast<BYTE>(b));
        }

    cleanup:
        return ret;
    }


    std::vector<std::string> WedpText::split(std::string& str, const char delim)
    {
        std::vector<std::string> ret;
        std::string curr;
        std::istringstream iss(str);

        while (std::getline(iss, curr, delim))
        {
            ret.push_back(curr);
        }
        return ret;
    }

    void WedpText::tolower(std::string& str)
    {
        for (size_t i = 0; i < str.size(); i++) {
            str[i] = static_cast<char>(std::tolower(str[i]));
        }
    }


    bool WedpText::file_output(std::string path, std::string file, std::string data)
    {
        bool ret = false;
        std::string fullpath;
        HANDLE hFile = INVALID_HANDLE_VALUE;
        DWORD dwWritten = 0;
        DWORD dwOut = 0;

        fullpath = path;

        // if we have \\ in the file it is  a path
        if (file.find('\\') != std::string::npos) {
            fullpath = file;
        }

        // Otherwise check the path var
        if (fullpath.empty()) {
            
            // If its empty get the users tmp dir
            CHAR tmp[MAX_PATH] = { 0 };
            if (!GetTempPathA(MAX_PATH, tmp)) {
                DBGPRINT("WedpText::file_output: No path provided and failed to get temp path\n");
                goto cleanup;
            }

            fullpath = std::string(tmp);
        }

        fullpath += "\\" + file;
        hFile = CreateFileA(fullpath.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
        if (INVALID_HANDLE_VALUE == hFile) {
            DBGPRINT("WedpText::file_output: Failed to create the file %s -- Error: %d\n", fullpath.c_str(), GetLastError());
            goto cleanup;
        }

        // Write all
        while (dwWritten < data.size()) {
            if (!WriteFile(hFile, data.c_str() + dwWritten, (DWORD)(data.size() - dwWritten), &dwOut, NULL)) {
                DBGPRINT("WedpText::file_output: Failed to write the file -- Error: %d\n", GetLastError());
                goto cleanup;
            }

            dwWritten += dwOut;
            dwOut = 0;
        }
        
        (*this) << "Data written to: " << fullpath << "\n";
        ret = true;

    cleanup:
        if (INVALID_HANDLE_VALUE != hFile) {
            CloseHandle(hFile);
        }

        return ret;
    }

    void WedpText::strip_front(std::string& str, const char c)
    {
        while ((!str.empty()) && str[0] == c) {
            str.erase(0, 1);
        }
    }

    void WedpText::strip_back(std::string& str, const char c)
    {
        while ((!str.empty()) && str[str.size() - 1] == c) {
            str.erase(str.size() - 1, 1);
        }
    }

    void WedpText::strip(std::string& str, const char c)
    {
        this->strip_front(str, c);
        this->strip_back(str, c);
    }

    // Stream friend to wrap output to debugger console
    WedpText& operator<<(WedpText& out, const std::string& mesg)
    {
        size_t start = 0;
        size_t size = 5000;

        while (start + size < mesg.size()) {
            EXT_INSTANCE.Out("%s", mesg.substr(start, size).c_str());
            start += size;
        }
        EXT_INSTANCE.Out("%s", mesg.substr(start).c_str());
        return out;
    }

    WedpText& operator<<(WedpText& out, const size_t& mesg)
    {
        EXT_INSTANCE.Out("%u", mesg);
        return out;
    }

    std::vector<BYTE> WedpText::addressToBytes(ULONG64 address, bool x86)
    {
        std::vector<BYTE> bytes(8);
        memcpy(bytes.data(), &address, sizeof(address));

        if (x86) {
            bytes.erase(bytes.begin() + 4, bytes.end());
        }
        
        std::reverse(bytes.begin(), bytes.end());
        return bytes;
    }

    bool WedpText::char_is_ascii_lowercase(const BYTE c)
    {
        return this->char_is_in_range(c, 97, 122);
    }

    bool WedpText::char_is_ascii_uppercase(const BYTE c)
    {
        return this->char_is_in_range(c, 65, 90);
    }

    bool WedpText::char_is_ascii_digit(const BYTE c)
    {
        return this->char_is_in_range(c, 48, 57);
    }

    bool WedpText::is_ascii_alphanum(const std::vector<BYTE>& bytes)
    {
        bool ret = false;

        for (BYTE b : bytes) {
            if (!(this->char_is_ascii_digit(b)     ||
                  this->char_is_ascii_lowercase(b) ||
                  this->char_is_ascii_uppercase(b)))
            {
                goto cleanup;
            }
        }
       
        ret = true;
    cleanup:
        return ret;
    }

    bool WedpText::is_ascii_printable(const std::vector<BYTE>& bytes)
    {
        return this->is_in_range(bytes, 32, 126);
    }
    
    bool WedpText::is_null_free(const std::vector<BYTE>& bytes)
    {
        bool ret = false;
        for (BYTE b : bytes) {
            if ((BYTE)0x00 == b) {
                goto cleanup;
            }
        }

        ret = true;
    cleanup:
        return ret;
    }

    bool WedpText::char_is_in_range(const BYTE c, ULONG start, ULONG end)
    {
        return (c >= start && c <= end);
    }

    bool WedpText::is_in_range(const std::vector<BYTE>& bytes, ULONG start, ULONG end)
    {
        bool ret = false;
        for (BYTE b : bytes) {
            if (!(b >= start && b <= end)) {
                goto cleanup;
            }
        }

        ret = true;
    cleanup:
        return ret;
    }
} // namespace wedp