#pragma once
#include <string>
#include <vector>
#include <windows.h>

namespace wedp {

    // Used to manipulate data into different formats and generate patterns
	class WedpText {

        public:
			const static std::string ascii_upper;
			const static std::string ascii_lower;
			const static std::string ascii_digit;
			const static std::string ascii_hex;

			// WedpText();
            std::string generatePattern(ULONG len, ULONG width = 0);
            ULONG patternOffset(std::string);
			std::string hexToAscii(std::string);
			// std::string asciiToHex(std::string);
			std::string bytesToHex(std::vector<BYTE>, std::string delim = std::string("\\x"));
			std::vector<BYTE> hexToBytes(std::string hex);
            std::vector<std::string> split(std::string& str, const char delim = ',');
            void tolower(std::string& str);

            void strip_front(std::string& str, const char c);
            void strip_back(std::string& str, const char c);
            void strip(std::string& str, const char c);

            std::vector<BYTE> addressToBytes(ULONG64 address, bool x86);
            bool char_is_ascii_lowercase(const BYTE);
            bool char_is_ascii_uppercase(const BYTE);
            bool char_is_ascii_digit(const BYTE);

            bool is_ascii_alphanum(const std::vector<BYTE>&);
            bool is_ascii_printable(const std::vector<BYTE>&);
            bool is_null_free(const std::vector<BYTE>&);

            bool file_output(std::string path, std::string file, std::string data);
            friend WedpText& operator<<(WedpText& out, const std::string& mesg);
            friend WedpText& operator<<(WedpText& out, const size_t& mesg);

        private:
            bool char_is_in_range(const BYTE c, ULONG start, ULONG end);
            bool is_in_range(const std::vector<BYTE>& bytes, ULONG start, ULONG end);
	};

} // namespace wedp