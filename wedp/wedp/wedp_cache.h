#pragma once
#include "wedp_module.h"

#include <list>
#include <Windows.h>

namespace wedp {

    // Template class to provide a generic interface for helper classes
    // that cache data for quicker access later
    template <typename V, typename S>
    class WedpCache {
        
        // ISSUE 14: need to rethink what interfaces should be public, private or protected
        public:

            // Check the validity of the cache
            bool isvalid() {
                return this->cache_is_valid;
            }

            // Get the full cache, prefer to use the filter
            const std::list<V>& getcache()
            {
                return this->cache;
            }

            // Set the cache to invalid
            void invalidate_cache() {
                this->cache_is_valid = false;
            }

            // Pure Virtual functions that sub classes must implement
            
            // Insert a value into the cache
            virtual bool insert(V val) = 0;

            // Checks the cache if an entry exists
            virtual bool exists(S val) = 0;

            // Get a value from the cache
            virtual const V *getvalue(S val) = 0;

            // Remove an item from the cache
            virtual bool remove(S val) = 0;

            // Filter the cache based on input options
            virtual std::vector<V> filter(WedpGenericOptions opts) = 0;
            
            // Check the validity of the cache
            virtual bool checkcachevalidity() = 0;

            // Update the caches
            virtual bool updatecache() = 0;
            
        protected:
            // Using a std::list here allows us to maintain references to objects in the list without
            // having to copy the objects. The insertion and removal times are constant, but the access times
            // are not. We prety much only access items through iteration over the full container so the slow
            // random access times do not matter in our case.
            std::list<V> cache;
            bool cache_is_valid;
            
            // Clear the cache map
            void clearcache()
            {
                cache.clear();
                this->cache_is_valid = false;
            }          
    };

} // namespace wedp