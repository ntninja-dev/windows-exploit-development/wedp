#pragma warning(disable: 4127)
#include <fort.hpp>
#pragma warning(default: 4127)

#include "wedp.h"
#include "wedp_memory.h"

#include <sstream>
#include <iomanip>

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

    // String lookup tables map memory information to their string representation
    std::map<INT, std::string> protectionsTable = {
        {PAGE_NOACCESS, std::string("PAGE_NOACCESS")},
        {PAGE_READONLY, std::string("PAGE_READONLY")},
        {PAGE_READWRITE, std::string("PAGE_READWRITE")},
        {PAGE_WRITECOPY, std::string("PAGE_WRITECOPY")},
        {PAGE_EXECUTE, std::string("PAGE_EXECUTE")},
        {PAGE_EXECUTE_READ, std::string("PAGE_EXECUTE_READ")},
        {PAGE_EXECUTE_READWRITE, std::string("PAGE_EXECUTE_READWRITE")},
        {PAGE_EXECUTE_WRITECOPY, std::string("PAGE_EXECUTE_WRITECOPY")}
    };

    std::map<INT, std::string> stateTable = {
        { MEM_COMMIT , std::string("MEM_COMMIT")},
        { MEM_RESERVE, std::string("MEM_RESERVE")},
        { MEM_FREE, std::string("MEM_FREE")}
    };

    std::map<INT, std::string> typeTable = {
        { MEM_IMAGE , std::string("MEM_IMAGE")},
        { MEM_MAPPED, std::string("MEM_MAPPED")},
        { MEM_PRIVATE, std::string("MEM_PRIVATE")}
    };

    // Check if a cached memory region is executable
    bool WEDP_MEMORY::isExecuatble()
    {
        bool ret = (this->protect & \
            (PAGE_EXECUTE | PAGE_EXECUTE_READ | PAGE_EXECUTE_READWRITE | PAGE_EXECUTE_WRITECOPY)) \
            != 0;

        return ret;
    }


    bool WedpMemoryHelper::insert(WEDP_MEMORY val)
    {
        bool ret = false;
        if (this->exists(val.base)) {
            goto cleanup;
        }

        this->cache.push_back(val);
        ret = true;

    cleanup:
        return ret;
    }
    
    
    bool WedpMemoryHelper::exists(ULONG64 val)
    {
        bool ret = false;
        for (WEDP_MEMORY mod : this->cache) {
            if (val == mod.base) {
                ret = true;
                goto cleanup;
            }
        }

    cleanup:
        return ret;
    }

    
    const WEDP_MEMORY *WedpMemoryHelper::getvalue(ULONG64 val)
    {
        const WEDP_MEMORY *ret = nullptr;

        std::list<WEDP_MEMORY>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).base) {
                ret = &(*it);
                goto cleanup;
            }
            else if (val > (*it).base && val < ((*it).base + (*it).size)) {
                ret = &(*it);
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }

    bool WedpMemoryHelper::remove(ULONG64 val)
    {
        bool ret = false;
        std::list<WEDP_MEMORY>::iterator it = this->cache.begin();
        while (it != this->cache.end())
        {
            if (val == (*it).base) {
                this->cache.erase(it);
                ret = true;
                goto cleanup;
            }

            it++;
        }

    cleanup:
        return ret;
    }


    std::vector<WEDP_MEMORY> WedpMemoryHelper::filter(WedpGenericOptions opts)
    {
        std::vector<WEDP_MEMORY> filtered;
        std::vector<WEDP_MODULE> mods;
        ULONG64 count = 0;

        if ((opts.modulesFlags & WedpGenericOptions::MODULE_FLAG_EXCLUDE) ||
            (opts.modulesFlags & WedpGenericOptions::MODULE_FLAG_INCLUDE))
        {
            mods = EXT_INSTANCE.h_Module.filter(opts);
        }

        for (WEDP_MEMORY mem : this->cache) {
            if (-1 != opts.addressProtections && !(mem.protect & opts.addressProtections)) {
                continue;
            }

            // I dont really like this goto here
            if (!mods.empty()) {
                for (WEDP_MODULE mod : mods) {
                    if (mod.base == mem.moduleBase) {
                        goto add_mem;
                    }
                }
                continue;
            }

        add_mem:
            filtered.push_back(mem);
            count++;
            if ((ULONG64)(-1) != opts.resultCount && count >= opts.resultCount) break;
        }

        return filtered;
    }

    // Current implementation will reload memory each time
    bool WedpMemoryHelper::checkcachevalidity()
    {
        this->cache_is_valid = false;
        return this->isvalid();
    }


    // Primary logic to read all accessible memory regions for usermode and
    // Gather information about the range
    bool WedpMemoryHelper::updatecache()
    {
        ULONG64 hProcess = 0;
        ULONG64 currAddress = 0;
        ULONG64 maxAddress = 0x7fff0000;
        SIZE_T mbisz = 0;
        bool ret = false;
        
        // Check if the cache is valid, return if it is and clear it if it is not
        if (!this->checkcachevalidity()) {
            this->clearcache();
        }
        else {
            ret = true;
            goto cleanup;
        }

        // Need the process handle for VirtualQueryEx
        if (FAILED(EXT_INSTANCE.m_System->GetCurrentProcessHandle(&hProcess))) {
            goto cleanup;
        }

        // Check the arch and update the max address to examine
        if (!(EXT_INSTANCE.IsCurMachine32() || EXT_INSTANCE.Is32On64())) {
            this->x64range = true;
            maxAddress = 0x7fffffff0000;
        }
        
        // Primary loop to examine the full usermode address space
        while (currAddress < maxAddress) {
            MEMORY_BASIC_INFORMATION64 mbi;
            WEDP_MEMORY currMem;

            mbisz = VirtualQueryEx((HANDLE)hProcess, (LPCVOID)currAddress, (PMEMORY_BASIC_INFORMATION)&mbi, sizeof(mbi));
            if (this->x64range && mbisz == sizeof(mbi)) {
                CREATE_WEDP_MEMORY(MEMORY_BASIC_INFORMATION64, mbi, currMem);
            }
            else if (mbisz == sizeof(MEMORY_BASIC_INFORMATION32)) {
                CREATE_WEDP_MEMORY(MEMORY_BASIC_INFORMATION32, mbi, currMem);
            }
            else {
                EXT_INSTANCE.Out("VQ Failure 0x%p\n", currAddress);
                break;
            }

            // Get the module base address if the address is in a module region
            currMem.moduleBase = EXT_INSTANCE.h_Module.getModuleBase(currMem.base);

            this->insert(currMem);
            currAddress += currMem.size;
        }

        this->cache_is_valid = true;
        ret = true;

    cleanup:
        return ret;
    }

    DWORD WedpMemoryHelper::getprotect(ULONG64 addr)
    {
        const WEDP_MEMORY* mem = this->getvalue(addr);
        if (nullptr == mem) {
            return 0xffffffff;
        }

        return mem->protect;
    }


    // Generate the output table of memory information
    std::string WedpMemoryHelper::getMemoryTable(WedpGenericOptions opts)
    {
        if (!this->updatecache()) {
            return std::string();
        }

        std::vector<WEDP_MEMORY> mems = this->filter(opts);
        fort::char_table tbl;
        DWORD c = 0;

        tbl << fort::header << "ID" << "Base" << "Size" << "State" << "Protection" << "Type" << "Module" << fort::endr;

        for (WEDP_MEMORY memData : mems) {
            std::stringstream ss;
            tbl << c++;
            ss << "0x" << std::setfill('0') << std::hex << std::setw(((this->x64range) ? 64 : 32) / 4) << std::hex << memData.base;
            tbl << ss.str();
            ss.str(std::string());

            ss << "0x" << std::setfill('0') << std::hex << std::setw(((this->x64range) ? 64 : 32) / 4) << std::hex << memData.size;
            tbl << ss.str();
            ss.str(std::string());

            try {
                tbl << stateTable.at(memData.state);
            }
            catch (std::out_of_range) {
                tbl << "";
            }

            try {
                // HELP: https://docs.microsoft.com/en-us/windows/win32/memory/memory-protection-constants

                // Check if the PAGE_GUARD flag is set
                if (memData.protect & PAGE_GUARD) {
                    std::string protect = protectionsTable.at(memData.protect & ~(PAGE_GUARD));
                    protect.append("|PAGE_GUARD");
                    tbl << protect;
                }
                else {
                    tbl << protectionsTable.at(memData.protect);
                }
                
            }
            catch (std::out_of_range) {
                tbl << "";
            }
            
            try {
                tbl << typeTable.at(memData.type);
            }
            catch (std::out_of_range) {
                tbl << "";
            }

            if (-1 != memData.moduleBase) {
                const WEDP_MODULE *modData = EXT_INSTANCE.h_Module.getvalue(memData.moduleBase);
                tbl << modData->name;
            }
            else {
                tbl << "";
            }
            
            tbl << fort::endr;
        }

        return tbl.to_string();
    }

    // Simple getter, maybe make the flag public
    bool WedpMemoryHelper::isx64()
    {
        return this->x64range;
    }
} // namespace wedp