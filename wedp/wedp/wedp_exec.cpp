#include "wedp.h"
#include <windows.h>

extern EXT_CLASS g_ExtInstance;
#define EXT_INSTANCE g_ExtInstance

namespace wedp {

    // Set the delimeter we use to determine the end of command output
    const std::string WedpExec::WedpExecCmd::endOutputDelim = std::string("--ENDOFOUTPUT--");

    WedpExec::WedpExecCmd::WedpExecCmd()
    {
        this->orig = NULL;
        this->executionCompleteEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
        DebugCreate(__uuidof(IDebugClient), (void**)&this->client);
        this->client->QueryInterface(__uuidof(IDebugControl), (void**)&this->control);
    }

    WedpExec::WedpExecCmd::~WedpExecCmd()
    {
        if (this->orig) {
            this->client->SetOutputCallbacks(this->orig);
        }
        if (this->executionCompleteEvent) {
            CloseHandle(this->executionCompleteEvent);
        }
        if (this->control) {
            this->control->Release();
        }
        if (this->client) {
            this->client->Release();
        }
    }

	// Interface methods taken from dumpstk sample in windbg sdk
	STDMETHODIMP WedpExec::WedpExecCmd::QueryInterface(THIS_ _In_ REFIID InterfaceId, _Out_ PVOID* Interface)
	{
        // Standard implementation of QueryInterface
		*Interface = NULL;

		if (IsEqualIID(InterfaceId, __uuidof(IUnknown)) ||
			IsEqualIID(InterfaceId, __uuidof(IDebugOutputCallbacks)))
		{
			*Interface = (IDebugOutputCallbacks*)this;
			this->AddRef();
			return S_OK;
		}
		else {
			return E_NOINTERFACE;
		}
	}

	STDMETHODIMP_(ULONG) WedpExec::WedpExecCmd::AddRef(THIS)
	{
        // No true refcount needed
		return 1;
	}

	STDMETHODIMP_(ULONG) WedpExec::WedpExecCmd::Release(THIS)
	{
        // No true refcount needed
		return 0;
	}

	STDMETHODIMP WedpExec::WedpExecCmd::Output(THIS_ _In_ ULONG Mask, _In_ PCSTR Text)
	{
        UNREFERENCED_PARAMETER(Mask);
        std::string input = std::string(Text);
        std::size_t off = std::string::npos;

        // search the chunk of input we got for out output delimiter
        off = input.find(WedpExecCmd::endOutputDelim);
        if (std::string::npos != off) {
            // if we found the delimiter we need to erase it
            input.erase(off, input.size());

            // add the last of the input after erasing the delimeter 
            // to the ouput stream
            this->outStream << input;

            // trigger that we are done executing the command
            SetEvent(this->executionCompleteEvent);
            goto cleanup;
        }

		this->outStream << input;

    cleanup:
		return S_OK;
	}


	std::string WedpExec::WedpExecCmd::exec(std::string command) 
	{
        std::string ret = std::string();
        
        // Get the original output callback
        if (FAILED(this->client->GetOutputCallbacks(&this->orig))) {
            goto cleanup;
        }

        // Set out output callback
		if (FAILED(this->client->SetOutputCallbacks(this))) {
			goto cleanup;
		}

        // Tack on our echo command for the final output
        command.append("; .echo ");
        command.append(WedpExecCmd::endOutputDelim);
        
        // Execute the native command
		if (FAILED(this->control->Execute(DEBUG_OUTCTL_THIS_CLIENT, command.c_str(), DEBUG_EXECUTE_DEFAULT))) {
			goto cleanup;
		}

        // Wait until our event triggers signaling that we have all the output
        // NOTE: handle a timeout and return values
        WaitForSingleObject(this->executionCompleteEvent, INFINITE);
        ret = this->outStream.str();

    cleanup:
        // Reset the original callback
        if (this->orig) {
            this->client->SetOutputCallbacks(this->orig);
            this->orig = NULL;
        }

        return ret;
	}

    // Method that creates a WedpExecCmd object and calls its exec method
    // to return the implant
    std::string WedpExec::exec(std::string command)
    {
        // Create a new WedpExecCmd object for each exec run
        WedpExec::WedpExecCmd cmd;
        return cmd.exec(command);
    }


} // namespace wedp